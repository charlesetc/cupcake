// swift-tools-version:4.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "cupcake",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "cupcake",
            targets: ["cupcake"]),
        .executable(
            name: "cupcake-exe",
            targets: ["cupcake-exe"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "tin",
            dependencies: []),
        .target(
            name: "standard",
            dependencies: []),
        .target(
            name: "batter",
            dependencies: ["standard", "tin"]),
        .target(
            name: "paper",
            dependencies: ["standard", "tin"]),
        .target(
            name: "oven",
            dependencies: ["tin", "paper"]),
        .target(
            name: "cupcake",
            dependencies: ["paper", "batter", "oven"]),
        .target(
            name: "cupcake-exe",
            dependencies: ["cupcake"]),
        .testTarget(
            name: "batterTests",
            dependencies: ["batter", "paper"]),
        .testTarget(
            name: "cupcakeTests",
            dependencies: ["cupcake"]),
    ]
)
