/*
  Batter is the module that contains all
  the type inference and type checking logic
  in cupcake.
*/

import standard
import tin


public enum TypeError: Error {
  case UndefinedVar(span: Span)
  case Unsolvable(recursive_type: Typelabel, in: Monotype)
  case Bug(explanation: String)
  case Mismatch(expected: Monotype, got: Monotype)
  case MissingField(object_constraint: Constraint, field: String, expected_type: Typelabel)
}


struct Polytype {
  let typelabel: Typelabel
  let quantified_vars: Set<Typevalue>
  let kinding_environment: KindingEnvironment

  func free_variables(graph: Graph, kenv: KindingEnvironment) -> Set<Typelabel> {
    return graph
      .find(typelabel)!
      .free_variables(graph: graph, kenv: kenv)
      .subtracting(quantified_vars)
  }
}

extension Monotype {
  func free_variables(graph: Graph, kenv: KindingEnvironment) -> Set<Typelabel> {
    switch self {
    case .Generic(let typelabel):
      var fvs = Set<Typelabel>()
      let constraint = kenv[typelabel]
      for (_, typelabel) in constraint?.relations ?? [:]  {
        fvs.formUnion(graph.find(typelabel)!.free_variables(graph: graph, kenv: kenv))
      }
      return fvs.union([typelabel])

    case .Arrow(let arg_label, let ret_label):
      let arg = graph.find(arg_label)!
      let ret = graph.find(ret_label)!
      return arg.free_variables(graph: graph, kenv: kenv)
      .union(ret.free_variables(graph: graph, kenv: kenv))

    case .Alias(let typelabel):
      return graph.find(typelabel)!
                  .free_variables(graph: graph, kenv: kenv)
    case .Base(_):
      return []
    }
  }
}

class Environment {

  init() {}

  // a list of frames: each a snapshot
  // of the variables added in that scope..
  var stack: [[String: Polytype]] = [[:]]

  subscript(varname: String) -> Polytype? {
    get {
      for frame in stack {
        if let type = frame[varname] {
          return type
        }
      }
      return nil
    }
    set(newtype) {
      stack[0][varname] = newtype
    }
  }

  func new_scope() {
    stack.insert([:], at: 0)
  }

  func drop_scope() {
    stack.remove(at: 0)
  }

  func free_variables(graph: Graph, kenv: KindingEnvironment) -> Set<Typelabel> {
    var out: Set<Typelabel> = []
    for frame in stack {
      for (_, polytype) in frame {
        out.formUnion(polytype.free_variables(graph: graph, kenv: kenv))
      }
    }
    return out
  }
}

class Graph: CustomStringConvertible {
  var graph: [Typelabel: Monotype] = [:]

  public var description: String {
    var out = "graph:\n---\n"
    graph
      .sorted { return $0.0 < $1.0 }
      .forEach {
        tuple in
        out += "\(tuple.0): \(tuple.1)\n"
      }
    out += "---"
    return out
  }

  subscript(index: Typelabel) -> Monotype? {
    get {
      return graph[index]
    }
    set(newtype) {
      graph[index] = newtype
    }
  }

  func find(_ typelabel: Typelabel) -> Monotype? {
    switch graph[typelabel] {
    case nil: return nil
    case .some(.Alias(let typelabel)):
      return find(typelabel)
    default:
      return graph[typelabel]
    }
  }

  func find_label(_ typelabel: Typelabel) -> Typelabel? {
    switch graph[typelabel] {
    case nil: return nil
    case .some(.Alias(let typelabel)):
      return find_label(typelabel)
    default:
      return typelabel
    }
  }
}

extension Monotype {
  func to_type(graph: Graph) -> Type {
    switch self {
    case .Alias(let label): return graph.find(label)!.to_type(graph: graph)
    case .Generic(let typevalue): return .Generic(typevalue)
    case .Base(let basetype): return .Base(basetype)
    case .Arrow(let arg_label, let ret_label):
      let arg = graph.find(arg_label)!.to_type(graph: graph)
      let ret = graph.find(ret_label)!.to_type(graph: graph)
      return .Arrow(arg: arg, ret: ret)
    }
  }
}

public class Typer {

  var graph = Graph()
  var kinding_environment = KindingEnvironment()
  var env = Environment()

  // only used when typechecking modules.
  var modules: [String: Module]?

  public func typecheck(module: Module) throws {

    // typecheck all the modules depth-first
    for (_, filename) in module.imported_modules {
      let imported = modules![filename]!
      try typecheck(module: imported)
    }

    // make a new scope
    env.new_scope()
    // make the import object for each module.
    for (module_name, filename) in module.imported_modules {
      let imported = modules![filename]!
      // make an object
      let associated_monotype = insert_new_generic()
      var constraint = Constraint(
        is_exact: true,
        labels: [],
        relations: [:]
      )
      for (fn_name, ast) in imported.exports {
        constraint.labels.insert(fn_name)
        constraint.relations[fn_name] = ast.typelabel!
      }
      kinding_environment[associated_monotype] = constraint

      let quantified_vars = graph.find(associated_monotype)!.free_variables(graph: graph, kenv: kinding_environment)
      print(quantified_vars)
      print(graph)

      // this might be wrong because it might need to use the kinding environment
      // from the subproblem. I'm not completely sure.
      env[module_name] = Polytype(
        typelabel: associated_monotype,
        quantified_vars: quantified_vars,
        // maybe not restrict it?
        kinding_environment: KindingEnvironment(of: kinding_environment, restricted_to: quantified_vars)
      )
    }

    // setup for typechecking this module
    for (name, toplevel) in module.exports {
      add_type_labels(tree: toplevel)
      let toplevel_label = graph.find_label(toplevel.typelabel!)!
      env[name] = Polytype(typelabel: toplevel_label, quantified_vars: [], kinding_environment: KindingEnvironment())
    }

    // actually do the typechecking
    for (name, toplevel) in module.exports {
      try typecheck_polymorphic_subproblem(root: toplevel, name: name)
    }

    // drop the imports we did.
    env.drop_scope()
  }

  public init(for_modules modules: [String: Module], starting_with first: Module) throws {
    self.modules = modules
    try typecheck(module: first)
  }

  public init(for_ast root: Ast) throws {
    try typecheck(tree: root)
  }

  init(as_subproblem_of other: Typer, root: Ast) throws {
    // assume the root already has typelabels.
    self.graph = other.graph
    self.env = other.env
    self.kinding_environment = other.kinding_environment
    // lack of reassignment means make a new kenv

    // reconstruct but don't do full typecheck, meaning
    // don't add in the types that they figured out
    // -- a subproblem is not complete.
    try reconstruct(tree: root)
  }

  func add_type_labels(tree root: Ast) {
    root.walk { ast in
      let index = unique_int()
      graph[index] = .Generic(index)
      ast.typelabel = index
    }
  }

  public func show_type(_ typelabel: Typelabel) throws -> String {
    switch graph.find(typelabel)! {
    case .Alias(_):
      throw TypeError.Bug(explanation: "find should never return an Alias")
    case .Generic(let i):
      return String(i)
    case .Base(let base_type):
      return String(describing: base_type)
    case .Arrow(let a, let b):
      return try "(" + show_type(a) + " -> " + show_type(b) + ")"
    }
  }

  func newlabel() -> Int {
    let u = unique_int()
    return u
  }

  func typecheck(tree: Ast) throws {
    add_type_labels(tree: tree)
    try reconstruct(tree: tree)
    add_actual_types(tree: tree)
  }

  func add_actual_types(tree: Ast) {
    tree.walk { ast in
      ast.type = graph.find(ast.typelabel!)!.to_type(graph: graph)
    }
  }

  func reconstruct(tree: Ast) throws {
    try tree.walk(
      before: reconstruct_single,
      after: reconstruct_after
    )
  }

  func reconstruct_after(ast: Ast) throws {
    switch ast.node {
    case .Lambda(_, _):
      env.drop_scope()
    default: ()
    }
  }

  func insert_new_generic() -> Typelabel {
    let new = newlabel()
    graph[new] = .Generic(new)
    return new
  }

  func insert_type(_ type: Monotype) -> Typelabel {
    let new = newlabel()
    graph[new] = type
    return new
  }

  func reconstruct_single(ast: Ast) throws {
    switch ast.node {
    case .Bool:
      let new = insert_type(.Base(.Bool))
      try unify(ast.typelabel!, new)
    case .Unit:
      let new = insert_type(.Base(.Unit))
      try unify(ast.typelabel!, new)
    case .Number:
      let new = insert_type(.Base(.Number))
      try unify(ast.typelabel!, new)
    case .String:
      let new = insert_type(.Base(.String))
      try unify(ast.typelabel!, new)
    case .Var:
      let name = String(describing: ast.span)
      guard let vartype: Polytype = env[name] else {
        throw TypeError.UndefinedVar(span: ast.span)
      }
      let theta = theta_from(vartype.quantified_vars)
      let new = newlabel_from( typelabel: vartype.typelabel, theta: theta)

      for quantified_var in vartype.quantified_vars {
        if let constraint = vartype.kinding_environment[quantified_var] {
          kinding_environment[theta(quantified_var)] = constraint.apply_theta(theta)
        }
      }

      try unify(ast.typelabel!, new)
    case .FnApp(let f, let x):
      let new = insert_type(.Arrow(arg: x.typelabel!, ret: ast.typelabel!))
      try unify(f.typelabel!, new)
    case .Chain(_, let second):
      try unify(ast.typelabel!, second.typelabel!)
    case .Lambda(let arg_names, let body_box):
      var last: Typelabel
      env.new_scope()

      if let body = body_box {
        last = body.typelabel!
      } else {
        last = insert_type(.Base(.Unit))
      }

      if arg_names == [] {
        let next_type: Monotype = .Arrow(arg: insert_type(.Base(.Unit)), ret: last)
        last = insert_type(next_type)
      }

      for name in arg_names.reversed() {
        let arg_label = insert_new_generic()
        // or should this use kinding_environment?
        env[name] = Polytype(typelabel: arg_label, quantified_vars: [], kinding_environment: KindingEnvironment())

        let next_type: Monotype = .Arrow(arg: arg_label, ret: last)
        last = newlabel()
        graph[last] = next_type
      }

      try unify(ast.typelabel!, last)
    case .Let(let name, let equal, in: let body_box):
      // in the initialization of the subproblem
      try typecheck_polymorphic_subproblem(root: equal, name: name)

      if let body = body_box {
        try unify(ast.typelabel!, body.typelabel!)
      } else {
        try unify(ast.typelabel!, insert_type(.Base(.Unit)))
      }
    case .Access(field: let field, of: let child):
      let associated_monotype = insert_new_generic()
      let constraint = Constraint(
        is_exact: false,
        labels: [field],
        relations: [field: associated_monotype]
      )
      kinding_environment[child.typelabel!] = constraint
      try unify(ast.typelabel!, associated_monotype)
    case .Object(let fields):
      let associated_monotype = insert_new_generic()
      var constraint = Constraint(
        is_exact: true,
        labels: [],
        relations: [:]
      )
      for (name, field_ast) in fields {
        constraint.labels.insert(name)
        constraint.relations[name] = field_ast.typelabel!
      }
      kinding_environment[associated_monotype] = constraint
      try unify(ast.typelabel!, associated_monotype)
    }
  }

  func typecheck_polymorphic_subproblem(root: Ast, name: String) throws {
      let subproblem = try Typer(as_subproblem_of: self, root: root)

      let kenv = subproblem.kinding_environment

      // subproblem.graph and graph should be one and the same.
      let monotype = graph.find(root.typelabel!)!
      let free_variables = monotype.free_variables(graph: graph, kenv: kenv)
      let environment_variables = env.free_variables(graph: graph, kenv: kenv)

      let quantified_vars = free_variables.subtracting(environment_variables)

      // same as monotype from before
      let root_label = graph.find_label(root.typelabel!)!

      env[name] = Polytype(
        typelabel: root_label,
        quantified_vars: quantified_vars,
        kinding_environment: KindingEnvironment(of: kenv, restricted_to: quantified_vars)
      )
      // update current kinding environment
      // maybe include kenv too?
      kinding_environment = KindingEnvironment(
        of: kinding_environment,
        restricted_to_not: quantified_vars
      )
  }


  func theta_from(_ quantified_vars: Set<Typevalue>) -> (Typelabel) -> Typelabel {
    var mapping: [Typelabel: Typelabel] = [:]
    for var_label in quantified_vars {
      mapping[graph.find_label(var_label)!] = insert_new_generic()
    }
    return { typelabel in
      mapping[self.graph.find_label(typelabel)!] ?? typelabel
    }
  }

  func newlabel_from(typelabel: Typelabel, theta: (Typelabel) -> Typelabel) -> Typelabel {
    switch graph.find(typelabel)! {
    case .Generic(let actual_label):
      return theta(actual_label)
    case .Alias(let nextlabel): // this is wrong.
      return newlabel_from(typelabel: nextlabel, theta: theta)
    case .Arrow(let arg, let ret):
      let new_arg = newlabel_from(typelabel: arg, theta: theta)
      let new_ret = newlabel_from(typelabel: ret, theta: theta)
      return insert_type(.Arrow(arg: new_arg, ret: new_ret))
    case .Base(_):
      return typelabel
    }
  }

  func throw_if_incompatible(_ label_a: Typelabel, _ label_b: Typelabel) throws {
    let (a, b) = (graph.find(label_a)!, graph.find(label_b)!) // repetitive
    switch (a, b) {
    case (.Base(_), .Base(_)): ()
    case (.Arrow(_, _), .Arrow(_, _)): ()
    case (.Generic(let label), _) where !have_kind(for: label): ()
    case (_, .Generic(let label)) where !have_kind(for: label): ()
    case (.Generic(let label_a), .Generic(let label_b))
      where have_kind(for: label_a) && have_kind(for: label_b): ()
    default:
      throw TypeError.Mismatch(expected: a, got: b)
    }
  }

  func unify(_ label_a: Typelabel, _ label_b: Typelabel) throws {
    let (a, b) = (graph.find(label_a)!, graph.find(label_b)!)
    // print(graph)
    // print("unify", a, b)
    if a == b { return }

    try throw_if_incompatible(label_a, label_b)

    // This switch is only to check for the unsolvable case.
    // An example is `: f { f }`
    switch (a, b) {
    case (.Generic(let alpha), let tau), (let tau, .Generic(let alpha)):
      if tau.free_variables(graph: graph, kenv: kinding_environment).contains(alpha) {
        throw TypeError.Unsolvable(recursive_type: alpha, in: tau)
      }
    default: ()
    }

    switch (a, b) {
    // These cases should never happen
    case (.Alias(_), _), (_, .Alias(_)):
      throw TypeError.Bug(explanation: "find should never return an Alias")
    case (.Base(_), .Arrow(_, _)), (.Arrow(_, _), .Base(_)):
      throw TypeError.Bug(
        explanation: "these are incompatible, should have been thrown in throw_if_incompatible")

    // Type check base types:
    case (.Base(let base_a), .Base(let base_b)):
      if base_a != base_b {
        throw TypeError.Mismatch(expected: a, got: b)
      }

    // Split on arrow types:
    case (.Arrow(let arg_a, let ret_a), .Arrow(let arg_b, let ret_b)):
      // print("splitting")
      try unify(arg_a, arg_b)
      try unify(ret_a, ret_b)

    // Two kinded variables:
    case (.Generic(let value_a), .Generic(let value_b)) where have_kind(for: value_a) && have_kind(for: value_b):
      let constraint_a = kinding_environment[value_a]!
      let constraint_b = kinding_environment[value_b]!

      let alpha = insert_new_generic()
      kinding_environment[alpha] = try constraint_a.wedge(constraint_b, unify: unify)

      kinding_environment[value_a] = nil
      kinding_environment[value_b] = nil
      // try unify(alpha, value_a)
      // try unify(alpha, value_b)
      graph[value_a] = .Alias(alpha)
      graph[value_b] = .Alias(alpha)

    // If only one of the generic variables is kinded, make the other an alias to the first.
    case (.Generic(let value_a), .Generic(let value_b)) where have_kind(for: value_a):
      graph[value_b] = .Alias(value_a)
    case (.Generic(let value_a), .Generic(let value_b)) where have_kind(for: value_b):
      graph[value_a] = .Alias(value_b)

    // The generic doesn't have a kind:
    case (.Generic(let value_a), _):
      // print("set \(value_a) to \(b)")
      graph[value_a] = .Alias(label_b)

    case (_, .Generic(let value_b)):
      // print("set \(value_b) to \(a)")
      graph[value_b] = .Alias(label_a)
    }
  }

  func have_kind(for typelabel: Typelabel) -> Bool{
    return kinding_environment[typelabel] != nil
  }
}
