
import tin

typealias Fieldlabel = String

public struct Constraint: Equatable {
  var is_exact: Bool
  var labels: Set<Fieldlabel>

  // This is slightly different than the
  // paper where the relations are kept
  // outside of the constraint.
  var relations: [Fieldlabel: Typelabel]

  func apply_theta(_ theta: (Typelabel) -> Typelabel) -> Constraint {
    var new_relations: [Fieldlabel: Typelabel] = [:]
    for (field, type) in relations { new_relations[field] = theta(type) }
    return Constraint(
      is_exact: is_exact,
      labels: labels,
      relations: new_relations
    )
  }

  func wedge(_ other: Constraint, unify: (Typelabel, Typelabel) throws -> ()) throws -> Constraint {
    var new_relations: [Fieldlabel: Typelabel]

    switch (self.is_exact, self, other.is_exact, other) {
    case (true, _, true, _), (false, _, false, _):
      new_relations = self.relations
      for (label, other_type) in other.relations {
        if let type = new_relations[label] {
          try unify(type, other_type)
        } else {
          new_relations[label] = other_type
        }
      }
    case (false, let inexact_one, true, let exact_one),
         (true, let exact_one, false, let inexact_one):
      new_relations = exact_one.relations
      for (label, other_type) in inexact_one.relations {
        if let type = new_relations[label] {
          try unify(type, other_type)
        } else {
          throw TypeError.MissingField(object_constraint: exact_one, field: label, expected_type: other_type)
        }
      }
    }

    return Constraint(
      is_exact: is_exact || other.is_exact,
      labels: labels.union(other.labels),
      relations: new_relations
    )
  }
}

struct KindingEnvironment {
   var mapping: [Typevalue: (Constraint)]

   init() {
     mapping = [:]
   }

   func join(_ other: KindingEnvironment) throws -> KindingEnvironment {
     var new = KindingEnvironment()
     for (typelabel, constraint) in mapping {
       new.mapping[typelabel] = constraint
     }
     for (typelabel, constraint) in other.mapping {
       if new.mapping[typelabel] != nil && new.mapping[typelabel] != constraint {
         throw TypeError.Bug(
           explanation: "I'm not sure what to do in this scenario -- see free variables definition in the paper"
         )
       }
       new.mapping[typelabel] = constraint
     }
     return new
   }

   init(of other: KindingEnvironment, restricted_to quantified_vars: Set<Typevalue>) {
     mapping = other.mapping
     for (mapped, _) in mapping {
       if !quantified_vars.contains(mapped) {
         mapping.removeValue(forKey: mapped)
       }
     }
   }

   init(of other: KindingEnvironment, restricted_to_not quantified_vars: Set<Typevalue>) {
     mapping = other.mapping
     for quantified in quantified_vars {
       mapping.removeValue(forKey: quantified)
     }
   }

  subscript(index: Typevalue) -> Constraint? {
    get {
      return mapping[index]
    }
    set(constraint) {
      mapping[index] = constraint
    }
  }
}
