
import paper
import batter
import oven
import Foundation

func prompt() {
  print("> ", terminator: "")
}

// TODO: eventually make this a
// fully-fledged repl and move
// to its own file.
// repl-type-print-loop
public func rtpl() {
  prompt()
  while true {
    do {
      var stdin = ""
      while let line = readLine() {
        if line == "q" || line == "exit" || line == "quit" {
          exit(0)
        }
        stdin += line + "\n"
        prompt()
      }
      let tree = try paper.read_tree(text: stdin)
      let typer = try Typer(for_ast: tree)
      print("\ntype: ", try typer.show_type(tree.typelabel!))
      print()
      prompt()
    } catch paper.ParseError.UnexpectedChar(let char, let location) {
      print()
      print("error: unexpected char \"\(char)\" at index \(location).")
      prompt()
      print()
    } catch let e {
      print()
      print(e)
      print()
      prompt()
    }
  }
}

public func bake(_ starting_point: String) throws {
  do {
    let oven = try Oven(start: starting_point)
    print(oven)
  } catch let e {
    print(e)
  }
}
