
import Foundation

import tin
import paper
import batter

public enum ModuleError: Error {
  case InvalidModuleName(String, in_file: String)
  case CannotFindModule(import_name: String, parent: String?)
  case CyclicModules
}

public class Oven: CustomStringConvertible {

  var modules: [String: Module] = [:]
  var first_module: Module?

  public var description: String {
    var out = ""
    out += "-- Oven --\n"
    for (_, module) in modules { out += "\(module)\n" }
    return out + "----------\n"
  }

  public init(start: String) throws {
    let first_import = "." + start
    first_module = try load_module(first_import)
    try check_for_cycles()
    let _ = try Typer(for_modules: modules, starting_with: first_module!)
  }

  func load_module(_ import_name: String, from: String? = nil) throws -> Module {
    let filename = try Path(import_name, from: from).filename()
    if let module = modules[filename] { return module }

    let module = try paper.read_module(filename: filename)
    modules[filename] = module
    try resolve_imports(module: module)
    return module
  }

  func resolve_imports(module: Module) throws {
    for (ident, import_name) in module.raw_imports {
      let newmodule = try load_module(import_name, from: module.filename)
      module.imported_modules[ident] = newmodule.filename
    }
  }

  func check_for_cycles() throws {
    try check_cycle(for_module: first_module!, parents: [])
  }

  func check_cycle(for_module m: Module, parents: [String]) throws {
    // depth first search of the graph
    // -- if ever a module is also one
    // of it's parents, then the graph
    // is cyclic.
    if parents.contains(m.filename) {
      throw ModuleError.CyclicModules
    }
    for (_, path) in m.imported_modules {
      let newm = modules[path]!
      let newparents = parents + [m.filename]
      try check_cycle(for_module: newm, parents: newparents)
    }
  }

}

class Path {

  let cupcake_ending: String = ".cupcake"
  let cupcake_install: String = "~/.cupcake/"
  let file_manager = FileManager.default


  let import_name: String
  let segments: [String]
  let from: String?
  let parent_components: ArraySlice<String>
  let parent_path: String

  init(_ import_name: String, from: String?) throws {
    self.from = from
    self.import_name = import_name

    segments = import_name.components(separatedBy: ".")

    parent_components = from?.components(separatedBy: "/")
                             .dropLast() ?? ["."]
    parent_path = parent_components.joined(separator: "/")

    try check_for_bad_input(segments)
  }

  func filename() throws -> String {
    var out: String
    // convert to path depending on whether
    // it's a relative import or an absolute one.
    if segments.first! == "" {
      let relative_path = segments.dropFirst().joined(separator: "/")
      out = parent_path + "/" + relative_path
    } else {
      if parent_components.contains(segments.first!) {
        let new_components = parent_components[parent_components.startIndex..<parent_components.index(of: segments.first!)!]
        out = (new_components + segments).joined(separator: "/")
      } else {
        out = cupcake_install + segments.joined(separator: "/")
      }
    }

    // If filename.cupcake exists then use it,
    // otherwise use filename/filename.cupcake.
    if file_manager.fileExists(atPath: out + cupcake_ending) {
      return out + cupcake_ending
    }
    out = out + "/" + segments.last! + cupcake_ending
    if file_manager.fileExists(atPath: out) {
      return out
    }
    print(out)
    throw ModuleError.CannotFindModule(import_name: import_name, parent: from)
  }


  func check_for_bad_input(_ segments: [String]) throws {
    for segment in segments.dropFirst() {
      if segment == "" {
        throw ModuleError.InvalidModuleName(import_name, in_file: from ?? "<top-level module>")
      }
    }
    if segments.count == 1 && segments.first == "" {
      throw ModuleError.InvalidModuleName(import_name, in_file: from ?? "<top-level module>")
    }
  }
}
