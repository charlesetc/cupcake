
/*
  Paper is the module that contains all the parsing and tokenizing logic for
  cupcake. Think of it as a wrapper for
  the main event.
*/

import Foundation
import standard
import tin

let alpha: [Character] =
  ["_",
   "a",
   "b",
   "c",
   "d",
   "e",
   "f",
   "g",
   "h",
   "i",
   "j",
   "k",
   "l",
   "m",
   "n",
   "o",
   "p",
   "q",
   "r",
   "s",
   "t",
   "u",
   "v",
   "w",
   "x",
   "y",
   "z"]

let numeric: [Character] =
  ["1",
   "2",
   "3",
   "4",
   "5",
   "6",
   "7",
   "8",
   "9",
   "0"]

public enum TokenNode: Equatable {
  case Space, Bool, Number, Ident, OpenRound, Let
  case CloseRound, OpenCurly, CloseCurly, Colon
  case Newline, Equals, DotField, FieldColon, Comma
  case Def, Require, String
}

struct Token: Equatable {
  var span: Span
  var node: TokenNode

  init(span: Span, node: TokenNode) {
    self.span = span
    self.node = node
  }
}

public enum TokenState {
  case Looking
  case Number
  case Ident
  case String, Escaped
  // This case is used to decide after a period
  // whether it is a field or not.
  case AfterDot
}

public enum ParseError: Error {
  case EOI(explanation: String)
  case ExpectedEOI(got: TokenNode, span: Span)
  case Expected(TokenNode, got: TokenNode?, span: Span)
  case UnexpectedChar(char: String.Element, location: Int)
  case UnexpectedToken(node: TokenNode, span: Span)
  case NoSuchFile(filename: String, span: Span?)
  case UnfinishedTokenEOI(state: TokenState)
  case Bug(explanation: String, span: Span)
}

class Tokener {
  let text: String
  var tokens: [Token] = []
  var location = 0
  var token_start = 0
  var state: TokenState = .Looking
  var char: Character?

  let keytokens: [String: TokenNode] = [
    " ": .Space,
    ",": .Comma,
    "(": .OpenRound,
    ")": .CloseRound,
    "{": .OpenCurly,
    "}": .CloseCurly,
    ":": .Colon,
    "\n": .Newline,
    ";": .Newline,
    "=": .Equals,
    "let": .Let,
    "true": .Bool, // you get the data from looking
    "false": .Bool, // at the accompanying span.
    "def": .Def,
    "require": .Require,
  ]

  init(text: String) {
    self.text = text
  }

  func read_keytoken() -> Token? {
    for (keytoken, node) in keytokens {
      let shadow = text.suffix(text.count - location).prefix(keytoken.count)
      if keytoken == shadow {
        let tok = Token(span: Span(location, location + keytoken.count-1, text: text), node: node)
        location += keytoken.count
        return tok
      }
    }
    return nil
  }

  func read() throws -> [Token] {
    while location < text.count {
      char = text[text.index(text.startIndex, offsetBy: location)]
      switch state {
      case TokenState.Looking:
        if let token = read_keytoken() {
          tokens.append(token)
          continue
        }
        // The starting point is always from the last looking state.
        token_start = location
        switch char! {
        case "a"..."z":
          state = .Ident
          location -= 1
        case "\"":
          state = .String
        case "0"..."9":
          state = .Number
          location -= 1
        case ".":
          if let peek_char = text[safe: location + 1] {
            if alpha.contains(peek_char) {
              token_start = location + 1 // ignore the dot
              state = .AfterDot
            } else { fallthrough }
          } else { fallthrough }
          state = .AfterDot
        default:
          throw ParseError.UnexpectedChar(char: char!, location: location)
        }
      case .String:
        if char == "\"" {
          location -= 1
          save(.String)
          location += 1
        } else if char == "\\" {
          state = .Escaped
        }
      case .Escaped:
        state = .String// basically continue reading.
      case .Number:
        munch_and_save(numeric, .Number)
      case .Ident:
        if char == ":" {
          // ignore the colon when saving the field
          location -= 1
          save(.FieldColon)
          location += 1
        } else {
          munch_and_save(alpha, .Ident)
        }
      case .AfterDot:
        munch_and_save(alpha, .DotField)
      }
      location += 1
    }
    if state != .Looking {
      throw ParseError.UnfinishedTokenEOI(state: state)
    }

    let output = tokens.filter { $0.node != .Space}
    return output
  }

  func munch_and_save(_ accepted: Array<Character>, _ node: TokenNode) {
    if !accepted.contains(char!) {
      location -= 1
      save(node)
    } else if location == text.count-1 {
      save(node)
    }
  }

  func save(_ node: TokenNode) {
    tokens.append(Token(span: Span(token_start, location, text: text), node: node))
    state = .Looking
  }

  func parse_bug() throws {
    throw ParseError.Bug(
      explanation: "There should not be any other states that read more than 1 char.",
      span: Span(token_start, location, text: text)
    )
  }

}

class Parser {
  let tokens: [Token]
  let text: String
  var index = 0
  var token: Token?

  init(text: String, tokens: [Token]) {
    self.tokens = tokens
    self.text = text
  }

  func peek() -> Token? {
    return tokens[safe: index]
  }

  func peek(at: Int) -> Token? {
    return tokens[safe: index + at ]
  }

  func advance() {
    index += 1
  }
  // This little function means we can call peek
  // in the construction of the parseast.
  func advance<T>(_ ast: T) -> T {
    advance()
    return ast
  }

  func read_tree() throws -> Ast {
    let ast = try expect(ast: read_expr())
    ignore_newlines()
    if index < tokens.count {
      // make sure there are no more close parens
      throw ParseError.ExpectedEOI(got: tokens[index].node, span: tokens[index].span)
    }
    return ast
  }

  func ignore_newlines() {
    while peek()?.node == .Newline {
      advance()
    }
  }

  func read_module(filename: String) throws -> Module {
    let module = Module(filecontents: text, filename: filename)
    while let (name, path) = try read_require() {
      module.raw_imports[name] = path
    }
    ignore_newlines()
    while let (name, ast) = try read_fndef() {
      module.exports[name] = ast
      ignore_newlines()
    }

    ignore_newlines()
    if index < tokens.count {
      // make sure there are no more tokens and the parse succeeded.
      throw ParseError.ExpectedEOI(got: tokens[index].node, span: tokens[index].span)
    }
    return module
  }

  func read_require() throws -> (String, String)? {
    if peek()?.node == .Require {
      advance()
      var path: String = ""
      var name: String = ""
      if peek()?.node == .Ident {
        path = read_string(token:.Ident)!
        name = path
      } else if peek()?.node != .DotField {
        try error(expected: .DotField)
      }
      while let field = read_string(token: .DotField) {
        path = path + "." + field
        name = field
      }
      return (name, path)
    }
    return nil
  }

  func read_fndef() throws -> (String, Ast)? {
    if peek()?.node == .Def {
      var args: [String] = []
      let first_span = peek()!.span
      advance()

      guard let fn_name = read_string(token: .Ident) else { try error(expected: .Ident) }
      while let arg = read_string(token: .Ident) {
        args.append(arg)
      }
      try expect_and_consume(node: .OpenCurly)
      let body = try read_expr()

      let last_span = peek()?.span
      try expect_and_consume(node: .CloseCurly)

      let ast = Ast(node: .Lambda(args, body), span: Span(first_span, last_span!))
      return (fn_name, ast)
    }
    return nil
  }

  func read_expr() throws -> Ast? {
    return try read_let()
  }

  func read_let() throws -> Ast? {
    if peek()?.node == .Let {
      let start_token = peek()!
      advance()
      guard let ident = read_string(token: .Ident) else { try error(expected: .Ident) }
      try expect_and_consume(node: .Equals)
      let expr = try not_eoi(read_fnapp(), because: "a let requires an expr after the equals")
      let body = try read_let_semi(leading_newline: true)
      let parsenode = AstNode.Let(ident, equal: expr, in: body)
      let span = Span(start_token.span, body?.span ?? expr.span)
      return Ast(node: parsenode, span: span)
    }
    return try read_let_semi(leading_newline: false)
  }

  func read_let_semi(leading_newline: Bool) throws -> Ast? {
    let first: Ast?

    if leading_newline {
      if peek() == nil || at_end_expression() { return nil }
      try expect_and_consume(node: .Newline) // and continue to the following:
      first = try read_let()
    } else {
      first = try read_fnapp()
    }
    if first == nil || peek() == nil || at_end_expression() {
      return first
    }
    try expect_and_consume(node: .Newline)
    guard let second = try read_let() else { return first }
    return Ast(node: .Chain(first!, second), span: Span(first!, second))
  }

  func at_end_expression() -> Bool {
    return peek()?.node == .CloseRound ||
           peek()?.node == .CloseCurly ||
           peek()?.node == .Comma
  }

  func read_fnapp() throws -> Ast? {
    guard let f = try read_access() else { return nil }
    return try read_fnapp_r(f: f)
  }

  func read_fnapp_r(f: Ast) throws -> Ast? {
    if let x = try read_access() {
      return try read_fnapp_r(f: Ast(node: .FnApp(f: f, x: x), span: Span(f, x)))
    } else {
      return f
    }
  }

  func read_access() throws -> Ast? {
    guard let accessed = try read_atom() else { return nil }
    return try read_access_r(accessed: accessed)
  }

  func read_access_r(accessed: Ast) throws -> Ast? {
    if peek()?.node == .DotField {
      let last_span = peek()!.span
      let field = read_string(token: .DotField)!
      return try read_access_r(accessed: Ast(
        node: .Access(field: field, of: accessed),
        span: Span(accessed.span, last_span)
      ))
    } else {
      return accessed
    }
  }

  func read_atom() throws -> Ast? {
    if index == tokens.count
       || peek()?.node == .Newline
       || at_end_expression() { return nil }
    let _ast = try
      read_number() ??
      read_bool() ??
      read_string_ast() ??
      read_var()
    let ast = try
      _ast ??
      read_lambda() ??
      read_object() ??
      read_parens()
    return try expect(ast: ast)
  }

  func read_number() throws -> Ast? {
    if peek()?.node == .Number {
      return advance(Ast(node: .Number, span: peek()!.span))
    }
    return nil
  }

  func read_bool() throws -> Ast? {
    if peek()?.node == .Bool {
      return advance(Ast(node: .Bool, span: peek()!.span))
    }
    return nil
  }

  func read_string_ast() throws -> Ast? {
    if peek()?.node == .String {
      return advance(Ast(node: .String, span: peek()!.span))
    }
    return nil
  }

  func read_var() throws -> Ast? {
    if peek()?.node == .Ident {
      return advance(Ast(node: .Var, span: peek()!.span))
    }
    return nil
  }

  func read_lambda() throws -> Ast? {
    var args: [String] = []
    let first_span = peek()!.span
    if peek()?.node == .Colon {
      advance()
      while let arg = read_string(token: .Ident) {
        args.append(arg)
      }
      try expect_and_consume(node: .OpenCurly)
    } else if peek()?.node == .OpenCurly {
      advance()
    } else {
      return nil
    }
    let body = try read_expr()
    let last_span = peek()?.span
    try expect_and_consume(node: .CloseCurly)

    return Ast(node: .Lambda(args, body), span: Span(first_span, last_span!))
  }

  func read_object() throws -> Ast? {
    var fields: [String: Ast] = [:]
    if peek()?.node == .OpenRound
        && peek(at: 1)?.node == .FieldColon {
      let first_span = peek()!.span
      repeat {
        advance()
        let field_name = read_string(token: .FieldColon)!
        fields[field_name] = try not_eoi(read_expr(), because: "you cannot have eoi in the middle of an object.")
      } while peek()?.node == .Comma
      let last_span = peek()?.span
      try expect_and_consume(node: .CloseRound)

      return Ast(node: .Object(fields), span: Span(first_span, last_span!))
    }
    return nil
  }

  func read_parens() throws -> Ast? {
    if peek()?.node == .OpenRound {
      let first_span = peek()!.span
      advance()
      if peek()?.node == .CloseRound {
        let second_span = peek()!.span
        advance()
        return Ast(node: .Unit, span: Span(first_span, second_span))
      }
      let expr = try not_eoi(read_expr(), because: "there is a parenthesis before the eoi")
      try expect_and_consume(node: .CloseRound)
      return expr
    }
    return nil
  }

  // util functions

  func expect_and_consume(node: TokenNode) throws {
    if peek()?.node != node {
      try error(expected: node)
    }
    advance()
  }

  func read_string(token: TokenNode) -> String? {
    if peek()?.node != token {
      return nil
    }
    return advance(String(describing: peek()!.span))
  }

  func not_eoi<T>(_ arg: T?, because explanation: String) throws -> T {
    if arg == nil {
      throw ParseError.EOI(
        explanation: explanation
      )
    }
    return arg!
  }

  func error(expected: TokenNode) throws -> Never {
    let got = peek()?.node
    let span =
      got == nil
      ? tokens[index - 1].span
      : peek()!.span
    throw ParseError.Expected(expected, got: got, span: span)
  }

  func expect(ast: Ast?) throws -> Ast {
    if ast == nil {
      if let token = peek() {
        throw ParseError.UnexpectedToken(node: token.node, span: token.span)
      } else {
        throw ParseError.EOI(explanation: "expected at least one token.")
      }
    }
    return ast!
  }
}

public func read_module(filename: String) throws -> Module {
  guard let text = try? String(contentsOfFile: filename) else {
    throw ParseError.NoSuchFile(filename: filename, span: nil)
  }

  let tokens = try Tokener(text: text).read()
  return try Parser(text: text, tokens: tokens).read_module(filename: filename)
}

public func read_tree(text: String) throws -> Ast {
  let tokens = try Tokener(text: text).read()
  return try Parser(text: text, tokens: tokens).read_tree()
}
