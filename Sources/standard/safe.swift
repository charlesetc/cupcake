
public extension Array {
  subscript (safe index: Int) -> Element? {
    return Int(index) < count ? self[Int(index)] : nil
  }
}

public extension String {
  subscript (safe i: Int) -> Element? {
    return Int(i) < count ? self[index(startIndex, offsetBy: i)] : nil
  }
}
