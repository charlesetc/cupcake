
fileprivate var index = 0
public func unique_int() -> Int {
  let out = index
  index += 1
  return out
}

public func reset_index_only_use_for_testing() {
  index = 0
}
