
/*
  Tin is the module that contains the major
  types used in the compiler: Span, Ast, etc.

  E.g. cupcake tin.
*/

// Types

// Used to signify that something
// is currently representing a generic
// typelabel, rather than a "concrete"
// one.
public typealias Typevalue = Int

public enum Basetype {
  case Number, Unit, Bool, String
}

// NB: Each typelabel refers
// to a monotype.

public enum Monotype: Equatable {
  case Generic(Typevalue)
  case Alias(Typelabel)
  case Arrow(arg: Typelabel, ret: Typelabel)
  case Base(Basetype)
}

// Ast

public struct Span: Equatable, CustomStringConvertible {
  let text: String
  var start: Int
  var end: Int
  public init(_ s: Span, _ e: Span) {
    text = s.text
    start = s.start
    end = e.end
  }
  public init(_ s: Ast, _ e: Ast) {
    text = s.span.text
    start = s.span.start
    end = e.span.end
  }
  public init(_ s: Int, _ e: Int, text: String) {
    self.text = text
    start = s
    end = e }
  public init(_ only_one: Int, text: String) {
    self.text = text
    start = only_one
    end = only_one
  }

  public var description: String {
    let start_index = text.index(text.startIndex, offsetBy: start)
    let end_index = text.index(text.startIndex, offsetBy: end)
    return String(text[start_index...end_index])
  }
}

public typealias Typelabel = Int

public indirect enum AstNode {
  case Bool, Number, Var, Unit, String
  case Lambda([String], Ast?)
  case FnApp(f: Ast, x: Ast)
  case Let(String, equal: Ast, in: Ast?)
  case Chain(Ast, Ast)
  case Object([String: Ast])
  case Access(field: String, of: Ast)
}

public class Ast: CustomStringConvertible {
  public let node: AstNode
  public let span: Span
  public var typelabel: Typelabel?
  public var type: Type?
  public init(node: AstNode, span: Span) {
    self.span = span
    self.node = node
  }

  public var description: String {
    var out: String
    switch node {
    case .Bool, .Number, .Var:
      out = String(describing: span)
    case .Unit:
      out = "()"
    case .String:
      out = "\"\(span.description)\""
    case .FnApp(let f, let x):
      out = "(\(f) \(x))"
    case .Let(let name, let equal, in: let body):
      if body == nil {
        out = "let \(name) = \(equal)"
      } else {
        out = "(let \(name) = \(equal); \(body!))"
      }
    case .Chain(let first, let second):
      out = "(\(first); \(second))"
    case .Lambda(let args, let body):
      out = ": "
      for arg in args { out += arg + " " }
      out += body == nil ? "{}" : "{ \(body!) }"
    case .Object(let fields):
      out = "("
      for (field, ast) in fields {
        out += "\(field): \(ast),"
      }
      out += ")"
    case .Access(field: let field, of: let ast):
      out = "(\(ast).\(field))"
    }
    if type != nil {
      out = String(describing: type!) + "::" + out
    } else if typelabel != nil {
      out = String(describing: typelabel!) + "::" + out
    }
    return out
  }

  public func walk(before: (Ast) throws -> ()) rethrows {
    try walk(before: before, after: { _ in ()})
  }

  public func walk(before: (Ast) throws -> (),
                   after: (Ast) throws -> ()
                   ) rethrows {
    try before(self)
    switch node {
    case .Bool, .Number, .Var, .Unit, .String: ()
    case .FnApp(let f, let x):
      try f.walk(before: before, after: after)
      try x.walk(before: before, after: after)
    case .Let(_, let equal, in: let body):
      try equal.walk(before: before, after: after)
      try body?.walk(before: before, after: after)
    case .Chain(let first, let second):
      try first.walk(before: before, after: after)
      try second.walk(before: before, after: after)
    case .Lambda(_, let body):
      try body?.walk(before: before, after: after)
    case .Access(field: _, of: let ast):
      try ast.walk(before: before, after: after)
    case .Object(let fields):
      for (_, ast) in fields {
        try ast.walk(before: before, after: after)
      }
    }
    try after(self)
  }
}

public class Module: CustomStringConvertible {
  public var raw_imports: [String: String] = [:] // imported name to relative module path
  public var exports: [String: Ast] = [:]

  // imported name to filename of module, which is also
  // the index into the oven's collection of modules.
  public var imported_modules: [String: String] = [:]

  public let filecontents: String
  public let filename: String
  public init(filecontents: String, filename: String) {
    self.filecontents = filecontents
    self.filename = filename
  }

  public var description: String {
    return "Module(filename: \(filename), imports: \(imported_modules))"
  }
}

public enum Type: Equatable, CustomStringConvertible {
  case Generic(Typevalue)
  indirect case Arrow(arg: Type, ret: Type)
  case Base(Basetype)

  public var description: String {
    switch self {
    case .Generic(let i):
      return String(i)
    case .Base(let base_type):
      return String(describing: base_type)
    case .Arrow(let a, let b):
      return "(" + String(describing: a) + " -> " + String(describing: b) + ")"
    }
  }
}
