import XCTest
@testable import cupcakeTests
@testable import batterTests

XCTMain([
    testCase(cupcakeTests.allTests),
    testCase(batterTests.allTests),
])
