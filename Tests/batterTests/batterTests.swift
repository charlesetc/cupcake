import XCTest
@testable import batter

import tin
import paper
import standard

func assert(_ text: String, has_type type: String) throws {
  let tree = try paper.read_tree(text: text)
  let _ = try batter.Typer(for_ast: tree)
  defer { reset_index_only_use_for_testing() }

  XCTAssertEqual(String(describing: tree.type!), type)
}

enum IntentionalError: Error {
  case WasSupposedToThrow(error: String)
}

func assert(_ text: String, fails_with error_repr: String) throws {
  defer { reset_index_only_use_for_testing() }
  do {
    let tree = try paper.read_tree(text: text)
    let _ = try batter.Typer(for_ast: tree)
  } catch let e {
    XCTAssertEqual(String(describing: e), error_repr)
    return // without throwing an error
  }
  throw IntentionalError.WasSupposedToThrow(error: error_repr)
}

class batterTests: XCTestCase {

  func testBasicToken() throws {
    try assert("2", has_type: "Number")
    try assert("()", has_type: "Unit")
    try assert("true", has_type: "Bool")
    try assert("false", has_type: "Bool")
  }

  func testFunctionLiteral() throws {
    try assert(": x { x }", has_type: "(2 -> 2)") // " syntax highlighting hack
    try assert(": x { }", has_type: "(2 -> Unit)") // "
    try assert(": { }", has_type: "(Unit -> Unit)") // "
    try assert("{ }", has_type: "(Unit -> Unit)") // "
    try assert(": { 2 }", has_type: "(Unit -> Number)") // "
    try assert("{ 2 ; true }", has_type: "(Unit -> Bool)") // "
  }

  func testLetBinding() throws {
    try assert("let x = 2", has_type: "Unit")
    try assert("let x = 2; x", has_type: "Number")
    try assert("let y = : x { x }; y", has_type: "(8 -> 8)") // "
    try assert("let x = : x { x }; x", has_type: "(8 -> 8)") // "

    try assert(
      """
      let id = : x { x }
      id 2
      """,
      has_type: "Number")

    try assert(
      """
      let id = : x { x }
      id true
      id
      """,
      has_type: "(16 -> 16)") // "

    try assert(
      """
      let id = : x { x }
      id 2
      id true
      """,
      has_type: "Bool")

    try assert(
      """
      let id = : x { x }
      id 2
      id true
      """,
      has_type: "Bool")

    try assert(
      """
      let f = : x { let f = 2 ; f }
      f
      """,
      has_type: "(14 -> Number)") // "

  }

  func testCommonFailures() throws {
    try assert("x", fails_with: "UndefinedVar(x)")
    try assert("(", fails_with: "EOI(\"there is a parenthesis before the eoi\")")
    try assert("())", fails_with: "ExpectedEOI(got: paper.TokenNode.CloseRound, span: ))")
    try assert("}", fails_with: "UnexpectedToken(node: paper.TokenNode.CloseCurly, span: })")
    try assert(":{", fails_with: "Expected(paper.TokenNode.CloseCurly, got: nil, span: {)")
    try assert(":{", fails_with: "Expected(paper.TokenNode.CloseCurly, got: nil, span: {)")
    try assert(": x { y }", fails_with: "UndefinedVar(y)")

    try assert(": x { x x }",
      fails_with: "Unsolvable(recursive_type: 3, in: tin.Monotype.Arrow(arg: 3, ret: 1))")

    try assert(": f { f 2 ; f true }",
      fails_with: "Mismatch(expected: tin.Monotype.Base(tin.Basetype.Number), got: tin.Monotype.Base(tin.Basetype.Bool))")

    try assert("(o: 2).honey",
      fails_with:
        "MissingField(object_constraint: batter.Constraint(is_exact: true, labels: Set([\"o\"]), relations: [\"o\": 2]), field: \"honey\", expected_type: 3)")
  }

  func testObjectLiterals() throws {
    try assert("(hi: 2, this: true)", has_type: "3")
    try assert("(wow: ())", has_type: "2")
    try assert("(wow: (this: true))", has_type: "3")

    try assert("""
      let x = true
      let y = (x: x)
      let x = (y: y)
      x.y.x
      """, has_type: "Bool")

    try assert("""
      let object = (this: 2, that: (honey: true))
      object.that.honey
      """, has_type: "Bool")
  }

  func testDotAccess() throws {
    // try assert("let x = 2; x.this.that", has_type: "7")
    try assert("(this_or_that: true).this_or_that", has_type: "Bool")
    try assert("let o = (length: 2); o.length", has_type: "Number")
  }

  func testFunctionsAndObjects() throws {
    try assert("""
      let o = : x { (honey: x) }
      (o true).honey
      """, has_type: "Bool")

    try assert("""
      let o = : x { (honey: x) }
      (o true).honey
      (o 2).honey
      """, has_type: "Number")

    try assert("""
      let id = : x { x }
      (id (hi: true)).hi
      """, has_type: "Bool")

    try assert("""
      let id = : x { x }
      (id (hi: true)).hi
      (id (hi: 2)).hi
      """, has_type: "Number")

    try assert("""
      let x = (this: : { 2 })
      x.this ()
      """, has_type: "Number")
  }

  static var allTests = [
    ("testBasicToken", testBasicToken),
    ("testFunctionLiteral", testFunctionLiteral),
    ("testLetBinding", testLetBinding),
    ("testCommonFailures", testCommonFailures),
    ("testDotAccess", testDotAccess),
    ("testObjectLiterals", testObjectLiterals),
    ("testFunctionsAndObjects", testFunctionsAndObjects),
  ]
}
