
# Immediate

## Features
[ ] better error messages for constraints
[ ] actually pretty-printing error messages for a span.
[ ] modules, with require
[ ] topological sort of top-level let statements so
    the programmer doesn't have to worry about their order.
[ ] require as

## Bugs
[ ] allow extra newlines in lots of places
[ ] span should keep the name of the file, not just the starting place.
[ ] union-find in type inference should implement common optimizations.

# Distant
[ ] while loops and for loops
[ ] classes
[ ] operators: +, -, \*, \=, ==, !=, -=, +=, \*=, /=, &&, ||, &, |
[ ] mutable variables
[ ] a backend :p
