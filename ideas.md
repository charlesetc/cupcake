
# Ideas

## Keyword arguments are records

Cupcake doesn't need extra syntax for keywords. Passing
an anonymous object into a function works just fine!
Then you can do destructuring in the let definition.

```
call_function_with_keywords (this: "2", other_keyword: 342)
```

## Classes

First of all, there is no inheritence. There could feasibly
be something along the lines of traits or mixins that can be
added to classes, some even automatically, in the future. Also
for code reuse there could be some macro-based system for
creating classes. For the moment, though, I want classes to
be quite simple: a class is a very special function definiton.
The initialier is based on the fields it comes with -- anything
that has a default value doesn't have to be included. The methods
are included "on" the object variable for type inference and the
like, but can be statically dispatched later.

### Class initializers

You can easily overload methods for class initialization, without
dyname dispatch. After unification, you can see what the type of the
method should be, and you already know the return type, so there should
not be much of a problem figuring out which to implement.


## Module System

Cupcake's module system is going to be file-based, at least
to start.

```
require mypackage
```

Mypackage will be represented in the type system
as an object

An interesting case comes up when assigning
and manipulating various packages as
if they are objects.

```
let package = if rand() > 0.5 {
  mypackage.startup
} else {
  mypackage.shutdown
}
package.run()
```

The solution, it seems, is to really make them
objects, and rely on the static dispatch that
will be guaranteed for all objects in cupcake
to be applied to the modules as well.

`require .this.that.woah`

If the pathname starts with a ".", it's a path
relative to the package at hand. When it
starts with a name, it represents an absolute
path from a top-level index of packages. When
cupcake looks for this.that, it will first see
if there are any modules in that path. I.e. any
file at "this/that/that.cupcake", and then look
for the file "this/that.cupcake". Finally, if
no such files are found, it will look for variables
named "that" in the "this" module.

One must specify all the names to be imported.

`require honey.utils` will generally just import
the name `utils`, but if that name is already taken
one can change this require statement to be
`require honey.utils as honey_utils`, or something
similar.

